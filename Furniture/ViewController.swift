//
//  ViewController.swift
//  Furniture
//
//  Created by Yefri Laura on 12/4/17.
//  Copyright © 2017 Yefri Laura. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    
    let URLBASE =  "http://45.55.79.245:8080/testUp/"
    
    var productosNow: NSArray = []
    var productosSale: NSArray = []
    var productosPopular: NSArray = []

    @IBOutlet weak var collectionPopular: UICollectionView!
    @IBOutlet weak var collectionSale: UICollectionView!
    @IBOutlet weak var collectionNow: UICollectionView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.collectionNow.dataSource = self
        self.collectionNow.delegate = self
        
        self.collectionSale.delegate = self
        self.collectionSale.dataSource = self
        
        self.collectionPopular.delegate = self
        self.collectionPopular.dataSource = self
        
        navigationController?.navigationBar.backgroundColor = UIColor(red:0.78, green:0.78, blue:0.78, alpha:1.0)
        navigationController?.navigationBar.shadowImage = UIImage()
        navigationController?.navigationBar.tintColor = UIColor.blue
        
        // Car
        let imageCar = UIImage(named: "shopping-cart");
        let buttomCar = UIBarButtonItem(image: imageCar, style: .plain, target: self, action: #selector(goCarro))
        navigationItem.rightBarButtonItem = buttomCar
        
        
        // User
        
        /*
        let imagePerfil = UIImage(named: "perfil");
        let buttomPerfil = UIBarButtonItem(image: imagePerfil, style: .plain, target: self, action: #selector(goCarro))
        navigationItem.leftBarButtonItem = buttomPerfil */

       listarProductosNow()
    }
    
    
    func listarProductosNow() {
        let urlNow = URLBASE + "producto/getProductosNow"
        
        if(NetworkManager.isConnectedToNetwork()){
            NetworkManager.sharedInstance.callUrlWithCompletion(url: urlNow, params: nil, completion: { (finished, response) in
                if(finished){
                    let result = NSMutableArray(array: response as NSArray)
                    
                    self.productosNow = result
                    
                    self.collectionNow.reloadData()
                }else{
                    /// error de conexion
                }
            }, method: .get)
        }else{
            // debo indicar que no hay internet
        }
        
        let urlSale = URLBASE + "producto/getProductosForSale"

        if(NetworkManager.isConnectedToNetwork()){
            NetworkManager.sharedInstance.callUrlWithCompletion(url: urlSale, params: nil, completion: { (finished, response) in
                if(finished){
                    let result = NSMutableArray(array: response as NSArray)
                    
                    self.productosSale = result
                    
                    self.collectionSale.reloadData()
                }else{
                    /// error de conexion
                }
            }, method: .get)
        }else{
            // debo indicar que no hay internet
        }
        
        let urlPopular = URLBASE + "producto/getProductosPopular"

        if(NetworkManager.isConnectedToNetwork()){
            NetworkManager.sharedInstance.callUrlWithCompletion(url: urlPopular, params: nil, completion: { (finished, response) in
                if(finished){
                    let result = NSMutableArray(array: response as NSArray)
                    
                    self.productosPopular = result
                    
                    self.collectionPopular.reloadData()
                }else{
                    /// error de conexion
                }
            }, method: .get)
        }else{
            // debo indicar que no hay internet
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @objc func goCarro(sender: UIBarButtonItem) {
        
        let vc = UIStoryboard(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "CarritoViewController") as! CarritoViewController
        
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 20, left: 20, bottom: 20, right: 20)
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView === self.collectionPopular {
            return productosPopular.count
        } else if collectionView === self.collectionSale {
            return productosSale.count
        } else {
            return productosNow.count
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        
        if collectionView === self.collectionPopular {
            DetailProductoController.producto = productosPopular[indexPath.row] as! Dictionary<String,AnyObject>
        } else if collectionView === self.collectionSale {
            DetailProductoController.producto = productosSale[indexPath.row] as! Dictionary<String,AnyObject>
        } else {
            DetailProductoController.producto = productosNow[indexPath.row] as! Dictionary<String,AnyObject>
        }
        
        let vc = UIStoryboard(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "detailProducto") as! DetailProductoController
        
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cellProducto", for: indexPath) as! CellProducto
        
        
        if collectionView === self.collectionPopular {
            cell.producto = productosPopular[indexPath.row] as! Dictionary<String,AnyObject>
        } else if collectionView === self.collectionSale {
            cell.producto = productosSale[indexPath.row] as! Dictionary<String,AnyObject>
        } else {
            cell.producto = productosNow[indexPath.row] as! Dictionary<String,AnyObject>
        }
                
        cell.initCell()
        
        return cell
    }


}

