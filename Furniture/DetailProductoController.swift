//
//  DetailProductoController.swift
//  Furniture
//
//  Created by Yefri Laura on 12/7/17.
//  Copyright © 2017 Yefri Laura. All rights reserved.
//

import UIKit
import SDWebImage

class DetailProductoController: UIViewController {

    
    static var producto: Dictionary<String,AnyObject>!
    @IBOutlet weak var d_producto: UIImageView!
    @IBOutlet weak var d_recomendaciones: UILabel!
    @IBOutlet weak var d_material: UILabel!
    @IBOutlet weak var d_marca: UILabel!
    @IBOutlet weak var d_tipo: UILabel!
    @IBOutlet weak var d_garantia: UILabel!
    @IBOutlet weak var d_stock: UILabel!
    @IBOutlet weak var d_precioAnterior: UILabel!
    @IBOutlet weak var d_descuento: UILabel!
    @IBOutlet weak var d_precio: UILabel!
    @IBOutlet weak var d_nombre: UILabel!
    

    override func viewDidLoad() {

        super.viewDidLoad()
        navigationController?.navigationBar.backgroundColor = UIColor.clear
        navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        navigationController?.navigationBar.shadowImage = UIImage()
        navigationController?.navigationBar.tintColor = UIColor.blue
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        

        createView()
    }
    
    func createView() {
        buildStyle()
        
        //let url = DetailProductoController.producto["imagen"] as! String
        let link = "https://s3.amazonaws.com/furniture1/imagenes/0302001.jpg"
        self.d_producto.sd_setImage(with: URL(string: link))
        
        let nombre = DetailProductoController.producto["nombre"] as! String
        self.d_nombre.text = nombre

        let descuentotext = DetailProductoController.producto["descuento"] as! Int
        self.d_descuento.text = "-\(descuentotext)%"
        
        let descuento = DetailProductoController.producto["descuento"] as! Double
        let precio = DetailProductoController.producto["precio"] as! Double
        let precioParse = precio - (precio * (descuento / 100))
        let d = String(format: "%.2f", precioParse);
        self.d_precio.text = "S/.\(d)"
        
        let sd = String(format: "%.2f", precio);
        self.d_precioAnterior.text = "Antes S/.\(sd)"
        
        let stock = DetailProductoController.producto["stockActual"] as! Int
        self.d_stock.text = "Hay \(stock) productos en stock."
        
        let garantia = DetailProductoController.producto["garantia"] as! Int
        self.d_garantia.text = "Tiene \(garantia) de garantía."

        let tipo = DetailProductoController.producto["tipo"]!["nombre"] as! String
        self.d_tipo.text = tipo
        
        let marca = DetailProductoController.producto["marca"]!["nombre"] as! String
        self.d_marca.text = marca
        
        let material = DetailProductoController.producto["material"]!["nombre"] as! String
        self.d_material.text = material

        let recomendaciones = DetailProductoController.producto["recomendacion"] as! String
        self.d_recomendaciones.text = recomendaciones

        
    }
    
    func buildStyle() {
        let colorB = UIColor(red:0.03, green:0.17, blue:0.73, alpha:1.0)
        let colorS = UIColor(red:0.54, green:0.95, blue:0.59, alpha:1.0)
        
        self.d_descuento.layer.cornerRadius = 3
        self.d_descuento.layer.masksToBounds = true
        self.d_descuento.backgroundColor = colorS
        self.d_descuento.textColor = colorB
        self.d_descuento.adjustsFontForContentSizeCategory = true
        self.d_descuento.adjustsFontSizeToFitWidth = true
        self.d_descuento.textAlignment = .center
        
        self.d_precio.layer.cornerRadius = 3
        self.d_precio.layer.masksToBounds = true
        self.d_precio.backgroundColor = colorB
        self.d_precio.textColor = colorS
        self.d_precio.adjustsFontSizeToFitWidth = true
        self.d_precio.adjustsFontForContentSizeCategory = true
        self.d_precio.textAlignment = .center
    }
 
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func agregarAlCarro(_ sender: Any) {
        let producto: Producto = DataBaseManager.shareInstance.crearLibro()
        
        let id = DetailProductoController.producto["idProducto"] as! String
        producto.codigo = id
        
        let nombre = DetailProductoController.producto["nombre"] as! String
        producto.nombre = nombre
        
        let imagen = DetailProductoController.producto["imagen"] as! String
        producto.imagen = imagen
        
        let descuento = DetailProductoController.producto["descuento"] as! Int
        producto.descuento = Int32(descuento)
        
        let precio = DetailProductoController.producto["precio"] as! Double
        producto.precio = precio
        
        let stock = DetailProductoController.producto["stockActual"] as! Int
        producto.stock = Int32(stock)
        
        DataBaseManager.shareInstance.SaveLibroDataBaseChanges()
        
        let _ = self.navigationController?.popViewController(animated: true)
        
    }

}
