//
//  CarritoViewController.swift
//  Furniture
//
//  Created by Yefri Laura on 12/10/17.
//  Copyright © 2017 Yefri Laura. All rights reserved.
//

import UIKit
import SDWebImage

class CarritoViewController: UIViewController {
    
    var furnitureData: NSArray = []

    @IBOutlet weak var table: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let notificationNme = NSNotification.Name("furnitureDelete")
        NotificationCenter.default.addObserver(self, selector: #selector(CarritoViewController.recargarLista), name: notificationNme, object: nil)
        
        let new: NSArray = DataBaseManager.shareInstance.getLibroArray()
        
        self.furnitureData = NSMutableArray(array: new)
        
        self.table.reloadData()

        // Do any additional setup after loading the view.
    }
    
    
    @objc func recargarLista() {
        
        let new: NSArray = DataBaseManager.shareInstance.getLibroArray()
        
        self.furnitureData = NSMutableArray(array: new)
        
        self.table.reloadData()
    }
     
    @IBAction func comprar(_ sender: Any) {
        DataBaseManager.shareInstance.deleteCompraTable()
        self.furnitureData = []
        self.table.reloadData()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

extension CarritoViewController: UITableViewDelegate, UITableViewDataSource{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.furnitureData.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let celda: CarritoTableViewCell = tableView.dequeueReusableCell(withIdentifier: "Celdita", for: indexPath) as! CarritoTableViewCell
        celda.producto = self.furnitureData[indexPath.row] as! Producto
            
        //scelda.delegate = self as! DailySpeakingLessonDelegate
    
        tableView.reloadRows(at: [indexPath], with: UITableViewRowAnimation.none)
        celda.ini()
        return celda
    }
    
    
    
    func dailySpeakingLessonButtonPressed() {
        self.table.reloadData()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        
    }
    
    
}


